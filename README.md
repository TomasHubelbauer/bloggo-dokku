# Dokku

- [ ] Check out Flynn for multiple server deployments:
  - [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-flynn)
  - [Bloggo post](http://hubelbauer.net/post/flynn)
- [ ] See if .NET Core can be hosted on Dokku
- [ ] See if removing Bloggo and redoing it as a non-default app, then changing it to default app will enable it to work with Let's Encrypt

- [x] Figure out how to deploy a database powered application to a Dokku subdomain without disrupting the default application
  - [x] Follow [the docs](http://dokku.viewdocs.io/dokku/deployment/application-deployment/):
  - [x] Create a [new application](https://gitlab.com/TomasHubelbauer/agenda/commit/f5900ae93f212ed077c35de88cb059495fd98ecd) (in this case a Node application)
  - [x] Shell to the host and run `dokku apps:create agenda`
  - [x] `sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git` to install Postgres
  - [x] Verify the default app still works
  - [x] `dokku postgres:create agenda-db` and note down the details
  - [x] `dokku postgres:link agenda-db agenda` and note the env var name and value
  - [x] Go back to local and `cd` into the application directory: `cd agenda`
  - [x] `git remote add dokku dokku@hubelbauer.net:agenda` to add a Dokku remote for the app (can check the remote in the default app)
  - [x] `wsl` to switch to Windows Subsystem for Linux because that's where the key is from when the default app was created
  - [x] `git push dokku master`
  - [x] Check the app runs at [`http://agenda.hubelbauer.net`](http://agenda.hubelbauer.net)
  - [x] Ensure the default app at [`http://hubelbauer.net`](http://hubelbauer.net) still works
  - [x] `dokku config:set --no-restart agenda DOKKU_LETSENCRYPT_EMAIL=tomas@hubelbauer.net`
  - [x] `dokku letsencrypt agenda`
  - [x] Check the app runs at [`http://agenda.hubelbauer.net`](http://agenda.hubelbauer.net) redirects to HTTPS
  - [x] Ensure the app runs at [`https://agenda.hubelbauer.net`](https://agenda.hubelbauer.net)
  - [x] Ensure the default app at [`http://hubelbauer.net`](http://hubelbauer.net) still works
  - [x] Update the app to connect to Postgres
    - [commit](https://gitlab.com/TomasHubelbauer/agenda/commit/8ad9efbd5c90847c755648e0c2fa14639ab3962e)
    - [errata](https://gitlab.com/TomasHubelbauer/agenda/commit/828ca069fa35fa9d9f43dfd3099d28fe1711aa6c)
    - [docs](https://node-postgres.com/features/connecting) (the environment variables is from the above)
  - [x] `wsl`
  - [x] `git push dokku master`
  - [x] Ensure the app runs at [`https://agenda.hubelbauer.net`](https://agenda.hubelbauer.net) and shows *Hello World* form the Postgres query
  - [x] Ensure the default app at [`http://hubelbauer.net`](http://hubelbauer.net) still works
  - [x] Rejoyce
  - Do a few more deployments with tweaks to the web app to show database data to see the data survives the deployments

Dokku is a self-hosted alternative to Heroku that allows one to just `git push dokku master` like with Heroku and have apps up in seconds.
It uses [herokuish](https://github.com/gliderlabs/herokuish) to simulate the Heroku build and deployment.

- [ViewTheDocs](http://dokku.viewdocs.io/dokku/)
- [GitHub](https://github.com/dokku/dokku/)

- [ ] See about the [Let's Encrypt issue](https://github.com/dokku/dokku-letsencrypt/issues/146) resolution
- [ ] Figure out how to [deploy apps to subdomains](http://dokku.viewdocs.io/dokku~v0.11.4/deployment/application-deployment/)

## Tips

### TypeScript

Running TypeScript applications on Dokku without any Procfiles can be achieved by running this in `start`:

```sh
tsc -p . && node src/index.js`
```

This will compile TypeScript to JavaScript (in the Dokku container only) and then run JavaScript.

It may be necessary to also run this once on the server:

```sh
dokku config:set --no-restart bloggo  DOKKU_DEFAULT_CHECKS_WAIT=60
```

This is so that TSC has time to build and Node to start, the default 10 seconds is very low for that.

**Note:** TypeScript needs to be in `dependencies` and not in `devDependencies` for this to work it appears.

### Kill stuck deployment

Kill app stuck in deployment: `rm /home/dokku/$app/.deploy.lock`

### Create a new app

- Log in to the host
- Create a new app: `dokku apps:create $name`
- `dokku config:set --no-restart $name DOKKU_LETSENCRYPT_EMAIL=tomas@hubelbauer.net`
- `dokku letsencrypt $name`
- Log out of the host
- Add the Dokku remote: `git remote add dokku dokku@hubelbauer.net:$name`

### Restarting

`dokku ps:restart <app>`

### View app logs

`dokku logs $name`

## Preparing

- Get a Ubuntu 16.04 VPS, e.g.: [Node Ubuntu VPS](https://gitlab.com/TomasHubelbauer/bloggo-node-ubuntu-vps),
- `ssh-keygen`

## Installing

- `ssh root@31.31.77.33`
- As per https://github.com/dokku/dokku#installation:
- `wget https://raw.githubusercontent.com/dokku/dokku/v0.12.7/bootstrap.sh`
- `sudo DOKKU_TAG=v0.12.7 bash bootstrap.sh`
- Wait…
- Go to [hubelbauer.net](http://hubelbauer.net) (or the serve IP if it has no DNS name associated)
- Enter the public key from `~/.ssh/id_rsa.pub`
- Enter the hostname `hubelbauer.net`
- Check *Use virtualhost naming for apps* (so that apps have their own subdomains)
- Click *Finish setup*
- `dokku apps:create bloggo` (on the server)
- `git remote add dokku dokku@hubelbauer.net:bloggo` (on the client)
- `git push dokku master`
- Enter passphrase for the key `~/.ssh/id_rsa.pub`

If asked for a password, do:

- `scp /home/Tom/.ssh/id_rsa.pub root@31.31.77.33:` on the client
- `dokku ssh-keys:add dokku id_rsa.pub` on the server

## Version Check

`dokku version`

## Securing

- `dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git`
- `dokku config:set --no-restart bloggo DOKKU_LETSENCRYPT_EMAIL=tomas@hubelbauer.net`
- `dokku letsencrypt bloggo`

- [ ] Figure out how to make this work on the default app
